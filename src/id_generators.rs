use uuid::{ Uuid };

pub fn java_session_id() -> String {
    return Uuid::new_v4().to_string();
}

use serde::{ Serialize };
use serde::de::{ DeserializeOwned };

use redis::{ Commands };
use r2d2::{ Pool };
use r2d2_redis::{ RedisConnectionManager };

pub struct RedisSessionStore {
    pub pool: Pool<RedisConnectionManager>
}

impl RedisSessionStore {
    pub fn from_pool(pool: Pool<RedisConnectionManager>) -> RedisSessionStore {
        return RedisSessionStore {
            pool: pool
        };
    }

    pub fn from_conn_manager(conn_manager: RedisConnectionManager) -> RedisSessionStore {
        return RedisSessionStore {
            pool: Pool::builder().build(conn_manager).unwrap()
        };
    }

    pub fn from_url(url: &str) -> RedisSessionStore {
        let conn_manager = RedisConnectionManager::new(url).unwrap();

        return RedisSessionStore {
            pool: Pool::builder().build(conn_manager).unwrap()
        };
    }

    pub fn get_state_of_session<SessionState: DeserializeOwned >(&self, session_id: &str) -> SessionState {
        let conn = self.pool.get().unwrap();

        let state: String = conn.get(session_id).unwrap();

        let result = serde_json::from_str(&state).unwrap();

        return result;
    }

    pub fn set_state_to_session<SessionState: Serialize>(&self, session_id: &str, state: &SessionState) {
        let conn = self.pool.get().unwrap();

        let state_json = serde_json::to_string(state).unwrap();

        let _: () = conn.set(session_id, state_json).unwrap();
    }
}
